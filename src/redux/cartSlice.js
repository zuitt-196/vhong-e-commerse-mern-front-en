import {createSlice} from "@reduxjs/toolkit";


const initialState = {
    products:[],
    quantity: 0,
    total: 0,

}

const cartSlice = createSlice({   
    name: 'cart',
    initialState,
    reducers:{
            ADD_PRODUCT:(state,action) =>{
                    // state.quantity = state.quantity + 1;
                    state.quantity += 1;
                    state.products.push(action.payload);
                    state.total += action.payload.price * action.payload.quantity;
             
            }
    }

})




export const {ADD_PRODUCT} = cartSlice.actions 

export default cartSlice.reducer;