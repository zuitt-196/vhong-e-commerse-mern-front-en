import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

// IMPORT THE GLOBLA STATE FOR ALL THE COMPONENTS HAVE ACCESS AND STATE MUTATE
import {Provider} from "react-redux";
import {store,persistor} from "./redux/store";


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

