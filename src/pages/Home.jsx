

import React from 'react'
import Annoucment from '../components/Annoucment'
import Catergories from '../components/Catergories'
import Footer from '../components/Footer'
import Nabvar from '../components/Nabvar'
import NewsLetter from '../components/NewsLetter'
import Products from '../components/Products'
import Slider from '../components/Slider'

const Home = () => {
  return (
    <div>
          <Annoucment/>
            <Nabvar/>
            <Slider/>
            <Catergories/>
            <Products/>
            <NewsLetter/>
            <Footer/>
            
    </div>
  )
}

export default Home