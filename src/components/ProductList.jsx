import React, {useState } from 'react'
import styled from 'styled-components'
import Navbar from '../components/Nabvar';
import Annoucment from '../components/Annoucment';
import Products from '../components/Products';
import NewsLetter from '../components/NewsLetter';
import Footer from '../components/Footer';
import { mobile } from '../responsive';
import {useLocation} from "react-router-dom"


const ProductList = () => {
                // define the URL path of category 
           const location = useLocation();
         const category = location.pathname.split("/" || " ")[2];
        

        // DEFINE THE FILTER STATE OF CATEGORY WITH THE VALUE OF OBJECT
        const [filters, setFilter] = useState({})

        // DEFIEN TRE SORT STATE 
        const [sort, setSort] = useState("newest")

      
        // console.log("filters:", filters);

        // DEFINE THE FUNCTION FOR STATE OF FLTERS
        const  handleFilters = (e) => {
                e.preventDefault();
                const value = e.target.value;
                // console.log("e.target.name:", e.target.name)
                // console.log("e.target.value:", e.target.value)
                setFilter({
                      ...filters , [e.target.name ] : value         
                }) ;
        }

          // DEFINE THE FUNCTION FOR SORTING
        
          const SortingHandle = (e) => {
                e.preventDefault()
                const value = e.target.value;
                setSort(value)
                
        }


  return (
    <Container>
        <Navbar/>
        <Annoucment/>
        <Title>{category}</Title>
        <FilterContainer>
                    <Filter>
                        <FilterText>Filter Products:</FilterText>
                        
                                        <Select  name="color" onChange={handleFilters}>
                                                <Option >
                                                        color
                                                </Option>
                                                <Option >
                                                        white
                                                </Option>
                                                <Option >
                                                        red
                                                </Option>
                                                <Option>
                                                        black
                                                </Option>
                                                <Option>
                                                        blue
                                                </Option>
                                                <Option>
                                                        yellow
                                                </Option>
                                                <Option>
                                                        green
                                                </Option>
                                        </Select>

                                        <Select name = "size"  onChange={handleFilters}>
                                                <Option disabled   defaultValue={'DEFAULT'}>
                                                        Size
                                                </Option >
                                                <Option>
                                                        SM
                                                </Option>
                                                <Option>
                                                        SL
                                                </Option>
                                                <Option>
                                                        L
                                                </Option>
                                                <Option>
                                                        XL
                                                </Option>
                                                <Option>
                                                        M
                                                </Option>
                                                
                                        </Select>
                    </Filter>

                    
                    <Filter>
                    <FilterText>Sort Products:
                    <Select onChange={SortingHandle}>  
                            <Option value = "newest" >
                                    Newest
                            </Option>
                            <Option value ="asc">
                                Price (asc)
                            </Option>
                            <Option value="desc">
                                    Price (desc)
                            </Option>
                           
                          </Select>  
                        
                     </FilterText>
                    </Filter>
        </FilterContainer>
        <Products category={category} filters={filters} sort={sort}/>
        <NewsLetter/>
        <Footer/>
    </Container>
  )
}

export default ProductList


const Container = styled.div`

`

const Title = styled.h1`
    margin: 20px;
`


const FilterContainer = styled.div`
    display:flex;
    justify-content: space-between;
    margin: 20px;
`

const Filter = styled.div`
        margin:20px;
        ${mobile({margin:"0px 20px", display: "flex" , flexDirection:"column"})};


`


const FilterText = styled.span`
    font-size: 20px;
    font-weight: 400;
    margin-right: 20px;
    ${mobile({marginRight:"0"})};
`

const Select = styled.select`
    padding: 10px;
    border:none;
    outline: none;
    margin-right: 20px;
    ${mobile({margin:"10px 0 "})};
`


const Option = styled.option``