import React from 'react'
import styled from 'styled-components'
import { categories } from '../data'
import CategoryItem from './CategoryItem'
// IMPORT RESPONSIVE
import {mobile} from '../responsive'

const Catergories = () => {
  return (
    <Container>
        {categories.map((item, index) => {
            return (
                <CategoryItem key={index} item={item}/> 
            )
        })}
    </Container>
  )
}

export default Catergories


const Container = styled.div`
    display:flex;
    padding: 40px;
    align-items:center;
    justify-content:center;
    ${mobile({padding:"0px",flexDirection: "column"})};
    
 

`