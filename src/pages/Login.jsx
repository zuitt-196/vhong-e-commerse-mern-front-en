import React,{useState} from 'react'
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
// IMPORT RESPONSIVE
import {mobile} from '../responsive'
import { login } from '../redux/apiCall';
import { useSelector } from 'react-redux';



const Login = () => {
      //DEFINE THE DISPATCH
      const dispatch = useDispatch();
      //DEFIFINE OR GET THE STATE OF USER USE THE useSeloctor hook
      const { isFetching,error} = useSelector(state=> state.user)
    ;
    // DEFINE THE USERNAME AND PASSWORD STATE WITH INITIALIZATION OF EMPTY STRING
        const [username, setUseranme] = useState("")
        const [password, setPassword] = useState("")
    
// DEFINE THE handleChange FUNCTION FOR USER

//  console.log(useranme, password)
//  DEFINEN THE FUNCRION FOR LOGIIN
const handleClick = (e) =>{
  e.preventDefault()
    login(dispatch, {username,password})
}




  return (
    <Container>
        <Wrapper>
            <Title>
                SIGN  IN 
            </Title>
            <Form>
                <Input placeholder="username" type="text" 
                  onChange={(e) =>setUseranme(e.target.value)}
                />
                <Input placeholder="password" 
                type ="password"
                  onChange={(e) => setPassword(e.target.value)}
                />

                <Button onClick={handleClick} disabled={isFetching}>LOG IN</Button>
                {/* DEFINE THE ERROR  MESSAGE FOR SOMETHNG HAPPEN WHEN YOUN LOG IN */}
               { error && <Error>Something went wrong....</Error>  }
                <Link>FORGOT PASSWORD </Link>
                <Link>CREATE A NEW ACCOUNT </Link>
            </Form>


        </Wrapper>


    </Container>
  )
}

export default Login;

const Container = styled.div`
  width: 100vw;
  height:100vh;
  background: linear-gradient(rgba(255,255,255,0.5),rgba(255,255,255,0.5)),url("https://images.pexels.com/photos/2112636/pexels-photo-2112636.jpeg?auto=compress&cs=tinysrgb&w=600"),center;                                          
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

    display:flex;
    align-items:center;
    justify-content: center;
  
`

const Wrapper = styled.div`
    padding:  20px;
    width: 25%;
    background:white;
    ${mobile({ width:"75%"})};

`



const Title = styled.h1`
  font-size:24px;
  font-weight:300;]


`

const Form = styled.form`
  display:flex;
  flex-direction:column;
  

`





const Input = styled.input`
    flex:1;
    min-width:40%;
    margin:20px 10px 0px 0px;
     padding: 10px;
     margin: 10px 0px;
`



const Button = styled.button`
  width:40%;
  border:none; 
  cursor:pointer;
  padding: 15px; 20px;
  background-color:teal;
  color:White;
  margin-bottom:10px;
  &:disabled{
      color: black;
      cursor:not-allowed;
  }

`
const Error  = styled.span`
  color:red;
`

const Link  = styled.a`
  margin: 5px 0px;
  font-size: 12px;
  text-decortion: underline;

`